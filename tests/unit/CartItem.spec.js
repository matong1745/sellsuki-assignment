import { shallowMount } from '@vue/test-utils'
import CartItem from '@/components/CartItem.vue'

let wrapper = null

afterEach(() => {
  wrapper.destroy()
})

describe('CartItem.vue', () => {
  it('renders props.data and index when passed', () => {
    const data = {
      title: 'Test',
      price: 100,
      cover: 'Image url',
      amount: 2,
    }
    const index = 0
    wrapper = shallowMount(CartItem, {
      propsData: { data, index },
    })
    expect(wrapper.find('.order').text()).toContain(index + 1)
    expect(wrapper.find('.book-title').text()).toMatch(data.title)
    expect(wrapper.find('.book-price').text()).toContain(data.price)
    expect(wrapper.find('.amount').text()).toContain(data.amount)
  })

  it('renders with amount equal to 1 delete_outline icon should be appear', () => {
    const data = {
      title: 'Test',
      price: 100,
      cover: 'Image url',
      amount: 1,
    }
    const index = 0
    wrapper = shallowMount(CartItem, {
      propsData: { data, index },
    })
    expect(wrapper.find('.delete_outline').exists()).toBe(true)
  })

  it('renders with amount more than 1 remove icon should be appear', () => {
    const data = {
      title: 'Test',
      price: 100,
      cover: 'Image url',
      amount: 3,
    }
    const index = 0
    wrapper = shallowMount(CartItem, {
      propsData: { data, index },
    })
    expect(wrapper.find('.remove').exists()).toBe(true)
  })
})
