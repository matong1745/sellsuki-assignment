import Vue from 'vue'
import Vuex from 'vuex'

import book from './book'
import cart from './cart'
import history from './history'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    book,
    cart,
    history,
  },
})

export const strict = false
