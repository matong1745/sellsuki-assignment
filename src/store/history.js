export default {
  namespaced: true,
  state: {
    data: [],
  },
  mutations: {
    addHistory(state, payload) {
      state.data = [
        ...state.data,
        payload,
      ]
    },
  },
  getters: {
    getHistoryById: (state) => (id) => state.data.find((history) => history.id === id),
  },
}
