import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/index.vue'),
  },
  {
    path: '/history/:id',
    name: 'HistoryById',
    component: () => import('@/views/history/_id.vue'),
  },
]

const router = new VueRouter({
  routes,
})

export default router
